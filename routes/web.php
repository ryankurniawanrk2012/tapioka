<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/uploadData', 'HomeController@uploadData')->name('list-pickup');
Route::post('/home/importExcel', 'HomeController@import_excel')->name('importExcel');

Route::get('/kelolamenu', 'KelolaMenu@index')->name('kelolamenu');
Route::get('/kelolamenu/addMenu', 'KelolaMenu@addMenu')->name('add_menu_list');


Route::get('/kelolarole', 'KelolaRole@index')->name('kelolarole');
