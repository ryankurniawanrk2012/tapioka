<!DOCTYPE html>
<html>
<head>
  <!-- Meta -->
  @include('layout/head')
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    @include('layout/navbar')

    <!-- Sidebar -->
    
    @include('layout/sidebartest')
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Delivery Report</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right d-none">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">DataTables</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Delivery Vendory</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                    <th>CSS grade</th>
                    <th>CSS grade</th>
                  </tr>
                  </thead>
                  <tbody>
                      <tr>
                        <td>04/12/2019</td>
                        <td>123456789</td>
                        <td>Body Gel</td>
                        <td>234234</td>
                        <td>1</td>
                        <td>Bapak Rio</td>
                        <td>0877876345</td>
                        <td>Jl Mawar Depok</td>
                        <td>NuSkin GdPlz</td>
                        <td>Sentul</td>
                        <td>JNE</td>
                        <td>06/12/2019</td>
                        <td>Delivered</td>
                      </tr>
    
                      <tr>
                          <td>04/12/2019</td>
                          <td>123456789</td>
                          <td>Body Gel</td>
                          <td>234234</td>
                          <td>1</td>
                          <td>Bapak Rio</td>
                          <td>0877876345</td>
                          <td>Jl Mawar Depok</td>
                          <td>NuSkin GdPlz</td>
                          <td>Sentul</td>
                          <td>JNE</td>
                          <td>06/12/2019</td>
                          <td>On Progress</td>
                        </tr>
    
                      </tbody>
                </table>
                <div class="container">
                  <div class="row">
                      <div class="col">
          
                      </div>
                      <div class="col">
                          
                      </div>
                      <div class="col-2 mt-4">
                          <a class="btn btn-warning" href="print_pdf.php" role="button"> <i class="fa fa-print" aria-hidden="true"></i> Download </a>
                      </div>
                  </div>
                </div>
                

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Footer -->
    @include('layout/footer')

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

<!-- Meta -->
@include('layout/js')

<!-- page script -->
@include('layout/pagescript')

</body>
</html>
