@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List Pickup</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              
              {{-- notifikasi form validasi --}}
                  @if ($errors->has('file'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('file') }}</strong>
                    </span>
                  @endif
              {{-- notifikasi sukses --}}
                  @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong>{{ $sukses }}</strong>
                    </div>
                  @endif

                  <button type="button" class="btn btn-primary mr-5" data-toggle="modal" data-target="#importExcel" style="margin-bottom: 10px;">
                    IMPORT EXCEL
                  </button>
               	
                  <!-- Import Excel -->
                  <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <form method="post" action="{{ route('importExcel')}}" enctype="multipart/form-data">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                          </div>
                          <div class="modal-body">
               
                            {{ csrf_field() }}
               
                            <label>Pilih file excel</label>
                            <div class="form-group">
                              <input type="file" name="file" required="required">
                            </div>
               
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Import</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    @php $i=1 @endphp
	                @foreach($siswa as $s)
						<tr>
							<td>{{ $i++ }}</td>
							<td>{{$s->nama}}</td>
							<td>{{$s->nis}}</td>
							<td>{{$s->alamat}}</td>
						</tr>
					@endforeach
                  </tr>
                  <tr>
                    
                  </tr>
                  </tbody>
                </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection