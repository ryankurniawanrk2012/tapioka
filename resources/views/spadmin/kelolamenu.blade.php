@extends('layoutAdmin.global')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1>Kelola Menu</h1>
	          </div>
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
<!-- 	              <li class="breadcrumb-item">Home</li>
	              <li class="breadcrumb-item active">DataTables</li> -->
	            </ol>
	          </div>
	        </div>
	      </div><!-- /.container-fluid -->
	    </section>

	<section class="content">
      	<div class="row">
        	<div class="col-12">
          		<div class="card">
            		<div class="card-header">
              			<h3 class="card-title">List Menu</h3>
            		</div> <!-- /.card-header -->
		            <div class="card-body">
		            	<a class="btn btn-primary float-right" href="{{ route('add_menu_list')}}" role="button">Add Menu</a>

						<table class="table table-striped">
							<thead>
								<tr>
									<th scope="col">No</th>
									<th scope="col">Nama Menu</th>
									<th scope="col">URI</th>
									<th scope="col">Icon</th>
									<th scope="col">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">1</th>
									<td>Mark</td>
									<td>Otto</td>
									<td>@mdo</td>
									<td>@mdo</td>
								</tr>
								<tr>
									<th scope="row">2</th>
									<td>Jacob</td>
									<td>Thornton</td>
									<td>@fat</td>
									<td>@fat</td>
								</tr>
								<tr>
									<th scope="row">3</th>
									<td>Larry</td>
									<td>the Bird</td>
									<td>@twitter</td>
									<td>@twitter</td>
								</tr>
							</tbody>
						</table>
		            </div>
		        </div>
		    </div>
		</div>
	</section>

	</div>

@endsection