@extends('layoutAdmin.global')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <!-- 
	           -->
	          <div class="col-sm-12">
	            <ol class="breadcrumb">
	              <li class="breadcrumb-item">Kelola Menu</li>
	              <li class="breadcrumb-item active">Create Menu</li>
	            </ol>
	          </div>
	        </div>
	      </div><!-- /.container-fluid -->
	    </section>

	<section class="content">
      	<div class="row">
        	<div class="col-12">
          		<div class="card">
            		<div class="card-header">
              			<h3 class="card-title">Create Menu</h3>					
            		</div> <!-- /.card-header -->
		            <div class="card-body">
		            	<form>
						<div class="form-group">
							<label for="formGroupExampleInput">Nama Menu</label>
							<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Menu">
						</div>
						<div class="form-group">
							<label for="formGroupExampleInput2">URL</label>
							<input type="text" class="form-control" id="formGroupExampleInput2" placeholder="URL">
						</div>
						<div class="form-group">
							<label for="formGroupExampleInput2">Icon</label>
							<input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Icon">
						</div>
						<div class="row">
							<div class="col-md-6">
								<a href="home.php" class="btn btn-success btn-block">Cancel</a>
							</div>
							<div class="col-md-6">
								<input type="Submit" name="registrasi" class="btn btn-success btn-block" value="Add">
							</div>
						</div>
						</form>

		            </div>
		        </div>
		    </div>
		</div>
	</section>

	</div>

@endsection