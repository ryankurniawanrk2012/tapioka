<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>NuSkin</title>
  <!-- CSRF Token -->
  <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
  @include('layoutAdmin.stylesheets')
</head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Site wrapper -->
      @include('layoutAdmin.nav-top')

      
      @include('layoutAdmin.sidebar-sadmi')
      

      
      {{-- @include('layoutAdmin.sidebar') --}} 
      

      @yield('content')

      @include('layoutAdmin.footer')

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
  <!-- /.control-sidebar -->

    <!-- ./wrapper -->
    @include('layoutAdmin.js')
  </body>
</html>
