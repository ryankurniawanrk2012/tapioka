<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Facade\Ignition\DumpRecorder\Dump;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SidebarController extends Controller
{
    public function getSidebar()
    {
        $sidebar = DB::table('sidebar')->get();

        
        return view('dashboard',['sidebar'=>$sidebar]);
    }
}
