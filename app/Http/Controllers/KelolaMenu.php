<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KelolaMenu extends Controller
{

    public function index() {

    	return view('spadmin.kelolamenu');
    }

    public function addMenu() {

    	 return view('spadmin.addmenu');
    }
}
