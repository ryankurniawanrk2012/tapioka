<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryVendor extends Model
{
    protected $table = "deliveryvendor";
    protected $fillable = ['date_order','so_number','item','no_aw'];
}
